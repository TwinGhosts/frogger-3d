using Assets.Scripts.World;
using System;
using UnityEngine;

public class WorldObject : MonoBehaviour
{
    public Action<Collider, Player> OnHit = (collider, player) => { };

    public void OnTriggerEnter(Collider collision)
    {
        var player = collision.GetComponent<Player>();
        OnHit.Invoke(collision, player);
    }
}
