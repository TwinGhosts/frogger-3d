using Assets.Scripts.Player;
using Assets.Scripts.World;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    public WorldBlock CurrentBlock { get; private set; }

    [SerializeField] private AnimalType avatar;
    [SerializeField] private LayerMask obstacleMask;

    [Title("Debug")]
    [SerializeField] private bool debug = false;

    private GameInput inputController;
    private Animator animator;
    private GameObject model;

    private bool canMove = true;
    private float animatorBaseSpeed = 1.5f;

    private void Start()
    {
        SetModel(avatar);

        animator.speed = animatorBaseSpeed;

        inputController = new GameInput();
        inputController.Enable();

        inputController.Game.Move.performed += (callbackContext) => Move(callbackContext);
        inputController.Game.Jump.performed += (callbackContext) => Jump(callbackContext);
    }

    private void LateUpdate()
    {
        if (!animator)
            return;

        if (animator.GetCurrentAnimatorStateInfo(0).IsName(Utility.AvatarController.AnimationList[3]) &&
            animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.99f)
        {
            animator.speed = 0f;
        }
        else if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f &&
            !animator.GetCurrentAnimatorStateInfo(0).IsName(Utility.AvatarController.AnimationList[8]))
        {
            animator.Play(Utility.AvatarController.AnimationList[8]);
        }
    }

    public void Scroll(float amount)
    {
        transform.position += -1f * amount * Time.deltaTime * Vector3.forward;
    }

    public void SetModel(AnimalType type)
    {
        if (model)
        {
            Destroy(model);
        }

        if (avatar != AnimalType.None)
        {
            model = Instantiate(Utility.AvatarController.GetAvatar(type));
            model.transform.parent = transform;
            model.transform.localPosition = Vector3.zero;
            model.transform.localScale = Vector3.one * 0.3f;

            animator = GetComponentInChildren<Animator>();
            animator.Play(Utility.AvatarController.AnimationList[8]);
        }
    }

    public void Hit()
    {
        Debug.LogWarning($"Player '{name}' has been hit!");
        animator.Play(Utility.AvatarController.AnimationList[3]);
    }

    private void Move(InputAction.CallbackContext callbackContext)
    {
        if (!canMove) return;

        var input = callbackContext.ReadValue<float>();
        var left = (input < -0.95f) ? Vector3.left : Vector3.zero;
        var right = (input > 0.95f) ? Vector3.right : Vector3.zero;
        var targetPosition = transform.localPosition;

        if (left != Vector3.zero && transform.position.x > -Utility.StageController.BlockPopulationWidth / 2f && !Physics.OverlapSphere(transform.position + Vector3.left, 0.4f, obstacleMask).Any())
        {
            animator.Play(Utility.AvatarController.AnimationList[9]);
            targetPosition += Vector3.left;
        }
        else if (right != Vector3.zero && transform.position.x < Utility.StageController.BlockPopulationWidth / 2f && !Physics.OverlapSphere(transform.position + Vector3.right, 0.4f, obstacleMask).Any())
        {
            animator.Play(Utility.AvatarController.AnimationList[10]);
            targetPosition += Vector3.right;
        }
        else return;

        animator.speed = 2f;
        canMove = false;

        transform.DOLocalMove(targetPosition, 0.225f)
            .SetEase(Ease.InOutCirc)
            .OnComplete(() =>
            {
                canMove = true;
                animator.speed = animatorBaseSpeed;
            });
    }

    private void Jump(InputAction.CallbackContext callbackContext)
    {
        if (!canMove)
            return;

        if (transform.position.z >= Utility.StageController.DeadzoneFront - 1f)
            return;

        animator.Play(Utility.AvatarController.AnimationList[11]);

        var input = callbackContext.ReadValue<float>();

        var targetPosition = transform.localPosition + Vector3.forward;
        var canMoveForward = !Physics.OverlapSphere(transform.position + Vector3.forward, 0.45f, obstacleMask).Any();
        var canMoveLeft = transform.position.x > -Utility.StageController.BlockPopulationWidth / 2f && !Physics.OverlapSphere(transform.position + Vector3.forward + Vector3.left, 0.45f, obstacleMask).Any();
        var canMoveRight = transform.position.x < Utility.StageController.BlockPopulationWidth / 2f && !Physics.OverlapSphere(transform.position + Vector3.forward + Vector3.right, 0.45f, obstacleMask).Any();
        var preferedMove = canMoveLeft ? Vector3.left : canMoveRight ? Vector3.right : Vector3.zero;

        if (canMoveLeft && canMoveRight && transform.position.x < 0)
        {
            preferedMove = Vector3.right;
        }

        if (!canMoveForward)
        {
            targetPosition += preferedMove;
        }

        if (preferedMove == Vector3.zero && !canMoveForward)
            return;

        canMove = false;

        transform.DOLocalMove(targetPosition, 0.25f)
            .SetEase(Ease.InOutCirc)
            .OnComplete(() =>
            {
                canMove = true;
                animator.speed = animatorBaseSpeed;
            });
    }

    private void OnDrawGizmos()
    {
        if (!debug) return;

        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position + Vector3.forward, 0.45f);
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position + Vector3.left, 0.4f);
        Gizmos.DrawSphere(transform.position + Vector3.right, 0.4f);
    }

    private void OnEnable()
    {
        inputController?.Enable();
    }

    private void OnDisable()
    {
        inputController?.Disable();
    }
}
