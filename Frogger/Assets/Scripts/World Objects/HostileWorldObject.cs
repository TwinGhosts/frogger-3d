using UnityEngine;

public class HostileWorldObject : WorldObject
{
    private void Awake()
    {
        OnHit += (collider, player) =>
        {
            if (player)
            {
                player.Hit();
            }
        };
    }
}
