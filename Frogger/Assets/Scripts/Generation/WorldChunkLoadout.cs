﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Generation
{
    [CreateAssetMenu(fileName = "New World Chunk Loadout", menuName = "World/New World Chunk Loadout", order = 1)]
    public class WorldChunkLoadout : ScriptableObject
    {
        public List<WorldRow> PopulationOptions = new List<WorldRow>();
    }
}
