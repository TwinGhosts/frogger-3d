﻿using Assets.Scripts.World;
using UnityEngine;

namespace Assets.Scripts.Generation
{
    public abstract class GenerationProfile : MonoBehaviour
    {
        public WorldBlock WorldBlock { get; private set; }

        protected void Awake()
        {
            WorldBlock = GetComponent<WorldBlock>();
        }

        public abstract void Generate();
    }
}
