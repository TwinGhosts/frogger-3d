﻿using Assets.Scripts.World_Objects;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Generation
{
    public class GrassGenerationProfile : GenerationProfile
    {
        [SerializeField] private List<TreeObject> treePrefabs = new List<TreeObject>();
        [SerializeField] private Vector2 scaleRange;
        [SerializeField] private float treeChance = 33f;

        public override void Generate()
        {
            for (var i = 0; i < WorldBlock.MaxWidth; i++)
            {
                var placeTree = Random.Range(0f, 100f) < treeChance;
                if (placeTree)
                {
                    var tree = Instantiate(treePrefabs[Random.Range(0, treePrefabs.Count)]);
                    var scale = Random.Range(scaleRange.x, scaleRange.y);
                    tree.transform.rotation = Quaternion.Euler(0f, Random.Range(0, 360f), 0f);
                    tree.transform.localScale = new Vector3(scale, scale, scale);
                    tree.transform.parent = WorldBlock.transform;
                    tree.transform.localPosition = new Vector3(-WorldBlock.MaxWidth / 2f + i, 0f, 0f);
                }
            }
        }
    }
}
