﻿using Assets.Scripts.Generation;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.World
{
    public class WorldBlock : MonoBehaviour
    {
        public Action OnSpawn = () => { };
        public Action OnDelete = () => { };

        public int MaxWidth { get; set; } = 10;
        public WorldBlock PreviousNeighbour { get; private set; }
        public WorldBlock NextNeighbour { get; private set; }
        public List<WorldObject> WorldObjects { get; private set; }

        [SerializeField] private GenerationProfile generationProfile;
        private float deletionDistance = -9999f;

        public void Populate()
        {
            generationProfile?.Generate();
        }

        public void SetObjects(List<WorldObject> worldObjects)
        {
            WorldObjects = worldObjects;
        }

        public void Move(float amount)
        {
            transform.position += Vector3.forward * amount * Time.deltaTime * -1f;
        }

        public void SetDeletionDistance(float deletionDistance)
        {
            this.deletionDistance = deletionDistance;
        }

        public void Delete()
        {
            OnDelete.Invoke();
            Destroy(gameObject);
        }

        public void SetNeighbours(WorldBlock previous, WorldBlock next)
        {
            PreviousNeighbour = previous;
            NextNeighbour = next;
        }

        private void Update()
        {
            if (transform.position.z < deletionDistance)
            {
                Delete();
            }
        }
    }
}
