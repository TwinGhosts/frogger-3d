﻿public enum LandType
{
    Grasslands,
    Road,
    Rails,
    Water,
}

public enum GrasslandsType
{
    None,
    Clear,
    Light,
    Dense,
}