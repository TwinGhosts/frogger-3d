﻿using Assets.Scripts.World;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Assets.Scripts.Generation
{
    [System.Serializable]
    public class WorldRow
    {
        [Range(0, 100)] public int Weight = 1;
        [AssetList] public WorldBlock WorldBlock;
    }
}
