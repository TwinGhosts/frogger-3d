﻿using Assets.Scripts.Player;
using System.Collections.Generic;
using UnityEngine;

public class AvatarController : MonoBehaviour
{
    [SerializeField] public List<GameObject> Avatars = new List<GameObject>();
    [SerializeField]
    public List<string> AnimationList = new List<string> {
                                                              "Attack",
                                                              "Bounce",
                                                              "Clicked",
                                                              "Death",
                                                              "Eat",
                                                              "Fear",
                                                              "Fly",
                                                              "Hit",
                                                              "Idle_A",
                                                              "Idle_B",
                                                              "Idle_C",
                                                              "Jump",
                                                              "Roll",
                                                              "Run",
                                                              "Sit",
                                                              "Spin/Splash",
                                                              "Swim",
                                                              "Walk"
                                                            };
    [SerializeField]
    public List<string> FacialExpList = new List<string> {
                                                              "Eyes_Annoyed",
                                                              "Eyes_Blink",
                                                              "Eyes_Cry",
                                                              "Eyes_Dead",
                                                              "Eyes_Excited",
                                                              "Eyes_Happy",
                                                              "Eyes_LookDown",
                                                              "Eyes_LookIn",
                                                              "Eyes_LookOut",
                                                              "Eyes_LookUp",
                                                              "Eyes_Rabid",
                                                              "Eyes_Sad",
                                                              "Eyes_Shrink",
                                                              "Eyes_Sleep",
                                                              "Eyes_Spin",
                                                              "Eyes_Squint",
                                                              "Eyes_Trauma",
                                                              "Sweat_L",
                                                              "Sweat_R",
                                                              "Teardrop_L",
                                                              "Teardrop_R"
                                                            };


    private void Awake()
    {
        Utility.AvatarController = this;
    }

    public GameObject GetAvatar(AnimalType type)
    {
        return Avatars[(int)type];
    }
}