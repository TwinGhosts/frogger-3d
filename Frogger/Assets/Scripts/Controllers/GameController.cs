using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [Title("References")]
    [SerializeField] private StageController stageController;

    private void Awake()
    {
        stageController.Initialize();
    }
}
