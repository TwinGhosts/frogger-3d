using Assets.Scripts.Generation;
using Assets.Scripts.World;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StageController : MonoBehaviour
{
    public int BlockPopulationWidth => blockPopulationWidth;
    public float DeadzoneFront => deathZoneFront;
    public float DeadzoneBack => deathZoneBack;

    [Title("Options", bold: true)]
    [Title("Players", horizontalLine: false, titleAlignment: TitleAlignments.Centered)]
    [SerializeField, Range(1, 4)] private int playerCount = 1;
    [SerializeField] private float playerSpawnDistance = 4f;

    [Title("World", horizontalLine: false, titleAlignment: TitleAlignments.Centered)]
    [SerializeField, Range(0.1f, 25f)] private float scrollSpeed = 2f;
    [SerializeField] private int initialWorldBlockCount = 15;
    [SerializeField] private int negativeBlocksUntilSpawn = 5;
    [SerializeField] private int numberOfEmptyRows = 3;
    [SerializeField] private float initialScrollDelay = 0.5f;
    [SerializeField] private float blockSize = 2f;
    [SerializeField] private int blockPopulationWidth = 12;
    [SerializeField] private float deathZoneFront = 15f;
    [SerializeField] private float deathZoneBack = -5f;

    [Title("References", bold: true)]
    [SerializeField] private Player playerPrefab;
    [SerializeField] private Transform worldBlockParent;
    [SerializeField] private WorldChunkLoadout worldChuckLoadout;
    [SerializeField] private WorldBlock emptyBlockPrefab;

    private List<Player> players = new List<Player>();
    private List<WorldBlock> activeWorldBlocks = new List<WorldBlock>();
    private List<WorldBlock> blockPrefabs = new List<WorldBlock>();

    public enum SpawnType
    {
        Default,
        Front,
        Last,
    }

    private void Awake()
    {
        Utility.StageController = this;
    }

    public void Initialize()
    {
        for (var i = 0; i < worldChuckLoadout.PopulationOptions.Count; i++)
        {
            var row = worldChuckLoadout.PopulationOptions[i];
            for (var j = 0; j < row.Weight; j++)
            {
                blockPrefabs.Add(row.WorldBlock);
            }
        }

        for (var i = 0; i < initialWorldBlockCount; i++)
        {
            if (i < numberOfEmptyRows)
            {
                SpawnEmptyWorldBlock();
            }
            else
            {
                SpawnWorldBlock();
            }
        }

        for (var i = 0; i < activeWorldBlocks.Count; i++)
        {
            var previousIndex = (int)Mathf.Repeat(i - 1, activeWorldBlocks.Count);
            var nextIndex = (int)Mathf.Repeat(i + 1, activeWorldBlocks.Count);

            var block = activeWorldBlocks[i];
            block.SetNeighbours(activeWorldBlocks[previousIndex], activeWorldBlocks[nextIndex]);
        }

        for (var i = 0; i < playerCount; i++)
        {
            var player = Instantiate(playerPrefab);
            player.transform.position = Vector3.forward * playerSpawnDistance + Vector3.forward;
            player.transform.parent = worldBlockParent;

            players.Add(player);
        }
    }

    public void SpawnWorldBlock(SpawnType type = SpawnType.Default)
    {
        var worldBlock = Instantiate(blockPrefabs[Random.Range(0, blockPrefabs.Count)]);
        worldBlock.transform.parent = worldBlockParent;
        worldBlock.transform.position = GetSpawnPositionFromType(type);
        worldBlock.SetDeletionDistance(negativeBlocksUntilSpawn);
        worldBlock.OnDelete += CheckBlockDeletion;
        worldBlock.MaxWidth = blockPopulationWidth;
        worldBlock.Populate();

        activeWorldBlocks.Add(worldBlock);
    }

    public void SpawnEmptyWorldBlock(SpawnType type = SpawnType.Default)
    {
        var worldBlock = Instantiate(emptyBlockPrefab);
        worldBlock.transform.parent = worldBlockParent;
        worldBlock.transform.position = GetSpawnPositionFromType(type);
        worldBlock.SetDeletionDistance(negativeBlocksUntilSpawn);
        worldBlock.OnDelete += CheckBlockDeletion;
        worldBlock.MaxWidth = blockPopulationWidth;
        worldBlock.Populate();

        activeWorldBlocks.Add(worldBlock);
    }

    private void Update()
    {
        initialScrollDelay -= Time.deltaTime;

        if (initialScrollDelay <= 0)
        {
            initialScrollDelay = 0;

            worldBlockParent.transform.position += Vector3.back * scrollSpeed * Time.deltaTime;
        }
    }

    private Vector3 GetSpawnPositionFromType(SpawnType type)
    {
        if (!activeWorldBlocks.Any())
            return Vector3.zero;

        switch (type)
        {
            default:
            case SpawnType.Default:
                return activeWorldBlocks.Count * Vector3.forward * blockSize; ;
            case SpawnType.Front:
                return activeWorldBlocks[0].transform.position + Vector3.forward * blockSize;
            case SpawnType.Last:
                return activeWorldBlocks[Mathf.Clamp(activeWorldBlocks.Count - 1, 0, int.MaxValue)].transform.position + Vector3.forward * blockSize;
        }
    }

    private void CheckBlockDeletion()
    {
        SpawnWorldBlock(SpawnType.Last);
    }

    private void OnDrawGizmos()
    {
        var leftTop = Vector3.zero + Vector3.left * blockPopulationWidth / 2f + Vector3.up * 0.1f + Vector3.forward * 100f + Vector3.left * 0.5f;
        var leftBot = Vector3.zero + Vector3.left * blockPopulationWidth / 2f + Vector3.up * 0.1f + Vector3.forward * -100f + Vector3.left * 0.5f;
        var rightTop = Vector3.zero + Vector3.right * blockPopulationWidth / 2f + Vector3.up * 0.1f + Vector3.forward * 100f + Vector3.left * 0.5f;
        var rightBot = Vector3.zero + Vector3.right * blockPopulationWidth / 2f + Vector3.up * 0.1f + Vector3.forward * -100f + Vector3.left * 0.5f;

        var backLeft = Vector3.zero + Vector3.forward * deathZoneBack + Vector3.left * 100f + Vector3.up * 0.1f;
        var backRight = Vector3.zero + Vector3.forward * deathZoneBack + Vector3.right * 100f + Vector3.up * 0.1f;
        var frontLeft = Vector3.zero + Vector3.forward * deathZoneFront + Vector3.left * 100f + Vector3.up * 0.1f;
        var frontRight = Vector3.zero + Vector3.forward * deathZoneFront + Vector3.right * 100f + Vector3.up * 0.1f;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(leftTop, leftBot);
        Gizmos.DrawLine(rightTop, rightBot);
        Gizmos.DrawLine(backLeft, backRight);
        Gizmos.DrawLine(frontLeft, frontRight);
    }
}
